#Makefile for serial

APP=serial

DEBUG=yes
SRC=src
BIN=bin
INCLUDE=include

clean:
		$(RM) -r $(BIN)

all: clean $(BIN)/$(APP)

test: all
		$(BIN)/$(APP)
		
.PHONY: clean all

		