/*
 * serialqueue.h
 *
 *  Created on: May 3, 2020
 *      Author: maria
 */


#ifndef SERIALQUEUE_H_
#define SERIALQUEUE_H_
/**
 *
 */
#include <sys/queue.h>

/**
\def MAX_DATA_LENGTH
 */
#define MAX_DATA_LENGTH 50

/**
\enum commands_t
 */
enum commands_t {
	CMD_UNKNOWN=0, CMD_START=1, CMD_STOP=2, CMD_GET_DATA=0x10, CMD_RESET=0xf
};

/**
 * serial packet_t structure
 * error: char array instead of char pointer
 */
struct serial_packet_t {
	int valid;
	char from_address;
	char to_address;
	char command;
	short data_length;
	char *data;
	TAILQ_ENTRY(serial_packet_t) next;
};

/**
 * \brief serial_init
 *
 * initialization of serial packet repository
 *
 * \result EXIT_SUCCESS if everything is ok
 */
int serial_init(int queue_size);

/**
 * \brief clear and free serial packet
 *
 * \result EXIT_SUCCESS if everything is ok
 */
int serial_deinit();

/**
 * \brief add a packet to the queue
 *
 * queue must be initialised
 * \param from_address
 * \param to address
 * \param command
 * \param data_length
 * \param data
 * \return EXIT_SUCCESS if successful
 * \return EXIT_FAILURE if unsuccessful
 */

/**
 * error: const char command instead of static int check_command
 */
int serial_add_packet(const char from_address, const  char to_address, int check_command, const short data_length, char *data);

/**
 * \brief size of queue
 *
 * \return result
 */
int serial_queue_size();

/**
 * \brief unused size of queue
 *
 * \return result
 */
int serial_unused_queue_size();

/**
 * \brief get a packet
 *
 * \return NULL if there is no packet
 *
 * \return packet if available
 */
struct serial_packet_t *serial_get_packet();

/**
 * \brief give back the processed packet
 *
 * \param packet
 *
 * \return
 */
int serial_give_back_processed_packet(struct serial_packet_t *packet);



#endif /* SERIALQUEUE_H_ */


