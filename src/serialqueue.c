/*
 * serialqueue.c

 *
 *  Created on: May 3, 2020
 *      Author: Maria and Funmi
*/


#include <stdlib.h>
#include <stdarg.h>
#include <strings.h>
#include <string.h>
#include <macros.h>
#include <serialqueue.h>
#include <pthread.h>
#include <syslog.h>
#include <unistd.h>

#define ERR_QUEUE_IS_NOT_INITIALIZED "Queue is not initailized yet"
#define ERR_MUTEX_INIT_FAILED "Mutex init failed"
#define ERR_MUTEX_LOCKING_FAILED "Locking mutex failed"
#define ERR_MUTEX_UN_LOCKING_FAILED "Unlocking mutex failed"
#define ERR_MUTEX_DESTROY "Mutex destroy failed"
#define ERR_UNKNOWN_COMMAND "Unknown command %d"
#define ERR_QUEUE_SIZE_IS_LESS_OR_EQUAL_ZERO "queue size is less or equal zero"
#define ERR_CANNOT_CREATE_EMPTY_PACKET_FOR_UNUSED "cannot create empty packet for unused queue"
#define ERR_DATA_LENGTH_IS_ZERO_BUT_DATA_IS_NOT_NULL "data length is zero but data is not null"
#define ERR_DATA_LENGTH_IS_NOT_ZERO_BUT_DATA_IS_NULL "data length is not zero but data is null"


#define DEBUG
#define NO_THREAD


static struct {
	TAILQ_HEAD(unused_pool, serial_packet_t) unused;
	TAILQ_HEAD(used_pool, serial_packet_t) used;
	int queue_size;
    int initialized;
    pthread_mutex_t lock;
} serial_queue = {
		.queue_size = 0,
		.initialized = 0,
		.lock = PTHREAD_MUTEX_INITIALIZER
};

/**
 * \Author:Maria
 */
static struct serial_packet_t *create_empty_packet() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter create_empty_packet");
#endif
	if (!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return NULL;
	}
	 struct serial_packet_t *packet;
	 MALLOC(packet, sizeof(struct serial_packet_t));
	 return packet;
}

/**
 * \Author:Funmi
 */
static void serial_free_packet(struct serial_packet_t *p) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_free_packet, p is null:%s", p?"false":"true");
#endif
	if (!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return;
	}
	if (!p) {
		syslog(LOG_DEBUG, "Packet to be free is null");
		return;
	}

	printf("Freed element is %d %d %d %s %d\n ", p->from_address, p->to_address, p->command,
			p->data, p->data_length);
		FREE(p);
}

/**
 * \Author:Maria
 */

/**
 * \brief dequeue_packet
 *
 * dequeuing a packet from unused pool
 *
 * \result the unused_packet if everything is ok
 */
struct serial_packet_t *dequeue_packet(){
	if (!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return NULL;
	}
	struct serial_packet_t *unused_p;
#ifndef NO_THREAD
	if(pthread_mutex_lock(&serial_queue.lock)) {
	    syslog(LOG_ERR,ERR_MUTEX_LOCKING_FAILED);
	    return NULL;
	 }
#endif
    unused_p=TAILQ_FIRST(&serial_queue.unused);
    TAILQ_REMOVE(&serial_queue.unused,unused_p,next);
#ifndef NO_THREAD
    if(pthread_mutex_unlock(&serial_queue.lock)){
       syslog(LOG_ERR,ERR_MUTEX_UN_LOCKING_FAILED);
       return NULL;
    }
#endif
    return unused_p;
}


int serial_init(int queue_size){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_init, queue_size:%d", queue_size);
#endif
	if (queue_size<=0) {
		syslog(LOG_ERR, ERR_QUEUE_SIZE_IS_LESS_OR_EQUAL_ZERO);
		return EXIT_FAILURE;
	}

	if (serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return EXIT_FAILURE;
	}

	serial_queue.queue_size = queue_size;
	TAILQ_INIT(&serial_queue.unused);
	TAILQ_INIT(&serial_queue.used);
	serial_queue.initialized=1;

	for (int i=0;i<queue_size;++i) {
		struct serial_packet_t *p = create_empty_packet();
		if (!p) {
			syslog(LOG_ERR, ERR_CANNOT_CREATE_EMPTY_PACKET_FOR_UNUSED);
			return EXIT_FAILURE;
		}
		TAILQ_INSERT_TAIL(&serial_queue.unused, p, next);
	}
#ifndef NO_THREAD
	if (pthread_mutex_init(&serial_queue.lock, NULL)) {
		syslog(LOG_ERR, ERR_MUTEX_INIT_FAILED);
		return EXIT_FAILURE;
	}
#endif
	return EXIT_SUCCESS;
}

int serial_deinit(){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_deinit");
#endif

	if (!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return EXIT_FAILURE;
	}

	struct serial_packet_t *p;
	while ((p=serial_get_packet()))
		serial_free_packet(p);

	while((p=TAILQ_FIRST(&serial_queue.unused))) {
		TAILQ_REMOVE(&serial_queue.unused, p, next);
		serial_free_packet(p);
	}
#ifndef NO_THREAD
	if (pthread_mutex_destroy(&serial_queue.lock)) {
		syslog(LOG_ERR, ERR_MUTEX_DESTROY);
		return EXIT_FAILURE;
	}
#endif
	return EXIT_SUCCESS;
}




/**
 * \Author:Maria
 */
int serial_queue_size() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_queue_size");
#endif
	if (!serial_queue.initialized) {
	     syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
	     return 0;
	}
	struct serial_packet_t *p;
	int result = 0;
	TAILQ_FOREACH(p, &serial_queue.used, next) {
				result++;
		}
	    return result;
}


/**
 * \Author:Maria
 */
int serial_unused_queue_size() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_unused_queue_size");
#endif
	 if (!serial_queue.initialized) {
	      syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
	      return 0;
	}
	struct serial_packet_t *p;
	int result = 0;
	TAILQ_FOREACH (p,&serial_queue.unused, next) {
			result++;
	}
	    return result;
}

/**
 * \Author:Maria
 */
struct serial_packet_t *serial_get_packet(){
#ifdef DEBUG
     syslog(LOG_DEBUG, "Enter serial_get_packet");
#endif
     if (!serial_queue.initialized) {
     	   syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
     	   return NULL;
      }
      struct serial_packet_t *p;
#ifndef NO_THREAD
      if(pthread_mutex_lock(&serial_queue.lock)){
          syslog(LOG_ERR,ERR_MUTEX_LOCKING_FAILED);
          return NULL;
     }
#endif
         p=TAILQ_FIRST(&serial_queue.used);
         if(!p){
#ifndef NO_THREAD
        	 pthread_mutex_unlock(&serial_queue.lock);
#endif
             return NULL;
          }
         TAILQ_REMOVE(&serial_queue.used,p,next);
#ifndef NO_THREAD
         pthread_mutex_unlock(&serial_queue.lock);
#endif
         return p;
}


/**
 * \Author:Funmi
 */

int check_command (const char command) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter check_command, command:%d",command);
#endif
	switch(command) {
	case CMD_GET_DATA: return 16;
	case CMD_RESET: return 15;
	case CMD_START : return 1;
	case CMD_STOP :  return 2;
	default : {
		syslog(LOG_WARNING, ERR_UNKNOWN_COMMAND, command);
		return 0;
		}
	}
}

/**
 * \Author: Funmi
 */
int serial_add_packet(const char from_address, const char to_address, int check_command, const short data_length, char *data) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_add_packet, from:%d, to:%d, command:%d, data_length:%d, data is null:%s",
					from_address, to_address, check_command, data_length, data?"false":"true");
#endif
	if(!serial_queue.initialized) {
			syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
			return EXIT_FAILURE;
		}
	if(!(from_address && to_address && check_command >= CMD_START &&
			check_command <= CMD_GET_DATA && data_length>=0
			&& data_length<MAX_DATA_LENGTH) && (data && *data))
			return EXIT_FAILURE;

	struct serial_packet_t *result=dequeue_packet();
	if(!result) {
		return EXIT_FAILURE;
	}
		result->from_address=from_address;
		result->to_address=to_address;
		result->command=check_command;
	if(!(data && *data) && data_length==0) {
			result->data_length=0;
			result->data=data;
		}
	if(data && *data && data_length>0) {
		result->data_length=data_length;
		memcpy(result->data, data, MAX_DATA_LENGTH);
	}
		int v=1;
		result->valid=v;
#ifndef NO_THREAD
	if(pthread_mutex_lock(&serial_queue.lock)) {
		syslog(LOG_ERR, ERR_MUTEX_LOCKING_FAILED);
		return EXIT_FAILURE;
	}
#endif
	TAILQ_INSERT_TAIL(&serial_queue.used, result, next);
#ifndef NO_THREAD
	pthread_mutex_unlock(&serial_queue.lock);
#endif
		return EXIT_SUCCESS;
}

/**
 * \Author:Funmi
 */
int serial_give_back_processed_packet(struct serial_packet_t *packet){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_give_back_processed_packet");
#endif
	if(!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return EXIT_FAILURE;
	}
	if(!packet) {
		syslog(LOG_DEBUG, "packet to be free is null");
		return EXIT_FAILURE;
	}
	int i=0;
		packet->valid=i;
#ifndef NO_THREAD
		pthread_mutex_lock(&serial_queue.lock);
#endif
			TAILQ_INSERT_TAIL(&serial_queue.unused, packet, next);
#ifndef NO_THREAD
		pthread_mutex_unlock(&serial_queue.lock);
#endif
		return EXIT_SUCCESS;
}
